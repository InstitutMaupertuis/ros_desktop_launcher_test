[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

This repository contains a [Robot Operating System](http://www.ros.org/) package to test ROS desktop launcher files.

<img src="/documentation/ubuntu_16.04.png" align="center" height="200">

<img src="/documentation/kubuntu_18.04.png" align="center" height="200">

# How to test
- Create a catkin work-space
- Clone this repository
```
git clone https://gitlab.com/InstitutMaupertuis/simple_desktop_launcher.git
```

- Install the work-space (`catkin_make` or `catkin tools`)
```
catkin_make install
catkin config --install && catkin build
```

- Desktop launcher files are installed in `$HOME/.local/share/applications/` if the `CATKIN_INSTAL_PREFIX` does not point in `/opt/`. In this case the application name is suffixed with `(debug)` to help differientate it from an installed package (see next line).
- Desktop launcher files are installed in `/usr/share/applications/` and `/usr//share/icons/` if `CATKIN_INSTAL_PREFIX` points in `/opt/`, this is useful if you are releasing your package and bloom sets the `CATKIN_INSTAL_PREFIX` to `/opt/`, this way the files will be automatically be installed at the right place..

Use your application menu to start the application. Alternatively `gtk-launch green_robot` and `gtk-launch red_robot` can be used.

The Unity dash usually takes a little bit of time (try logging-out/logging-in again) to update the available applications so it may not appear directly after install.

# How it works
`.desktop` and `.png` files are installed, these files are parsed by Ubuntu and create applications.

The image file is the icon of the application displayed in the application launcher.

These files are automatically installed via CMake when install is required.

Here are the steps in the CMake script:
- [Determine whether we will be installing in /opt or elsewhere and set `DESKTOP_APP_DIR` and `DESKTOP_ICON_DIR` accordingly](/CMakeLists.txt#L33-43)
- [Make sure the `$HOME/.local/share/applications/` exists (create it if necessary)](/CMakeLists.txt#L45-51)
- [Configure / install the `bash.in` scripts that run the applications](/CMakeLists.txt#L53-72): CMake variables like `${PROJECT_NAME}` are replaced by CMake.
- [Configure the `desktop.in` files and install them](/CMakeLists.txt#L74-89): CMake variables like `${PROJECT_NAME}` are replaced by CMake.
- [Copy application icon](/CMakeLists.txt#L91-96): CMake variables like `${PROJECT_NAME}` are replaced by CMake.

- [Install ROS config, launch, urdf files](/CMakeLists.txt#L111-119): `install(DIRECTORY)` is recursive and preserves directory structure. You may want to copy additionnal directories (eg: `meshes`) depending on your package.

## Bash scripts
The bash script automatically checks if a ROS core is running, if not one is started as a background task, remaining open even if the application is exited.
